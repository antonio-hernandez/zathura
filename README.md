# Zathura config,
Created with information found in many sources, including the man pages

## This attempts to use solarized dark and light.  

First change to the .config directory

  ```
  cd ~/.config/
  ```

  Clone the repository.
  
  You end up with three zathurarc files.  The actual config file for zathura which is zathurarc and the file to change the config to either solarized dark or solarized light.

  ~/.config/zathura/

  contains three files:

    zathurarc (defualt is solarized dark)
    zathurarc-solarized-light
    zathurarc-solarized-dark
    
   To change to solarized light.
   ```
   cp zathura-solarized-light zathurarc
   ```

   To change to solarized dark.
   ```
   cp zathura-solarized-dark zathurarc
   ```
   
   
